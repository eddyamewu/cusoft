﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for info_query_page.xaml
    /// </summary>
    public partial class info_query_page : Page
    {
        public info_query_page()
        {
            InitializeComponent();

            //user_info_frame.Content = new graph_charts_page();

            List<info> info = new List<info>();

            info.Add(new info() { Id = "1", Name = "Abdul Wahab Awudu", Acc_no = "O42-3599502" });

            info.Add(new info() { Id = "2", Name = "Fred Annan", Acc_no = "O42-3599502" });
            info.Add(new info() { Id = "3", Name = "Abdul Wahab Awudu", Acc_no = "O42-3599502" });
            info.Add(new info() { Id = "4", Name = "Fred Annan", Acc_no = "O42-3599502" });
            info.Add(new info() { Id = "5", Name = "Abdul Wahab Awudu", Acc_no = "O42-3599502" });
            info.Add(new info() { Id = "6", Name = "Fred Annan", Acc_no = "O42-3599502" });
            info.Add(new info() { Id = "7", Name = "Abdul Wahab Awudu", Acc_no = "O42-3599502" });
            info.Add(new info() { Id = "8", Name = "Fred Annan", Acc_no = "O42-3599502" });
            info.Add(new info() { Id = "9", Name = "Abdul Wahab Awudu", Acc_no = "O42-3599502" });
            datagrid.ItemsSource = info;

        }

        public class info
        {
            public string Id { get; set; }

            public string Name { get; set; }

            public string Acc_no { get; set; }
        }


        private void datagrid_dbl_click(object sender, RoutedEventArgs e)
        {
            full_info full_info = new full_info();

            NavigationService.Navigate(full_info);
        }



    }
}
