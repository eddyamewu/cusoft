﻿using Cusoft.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Cusoft.Data
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext() : base("Default Connection") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }


        public DbSet<Customer> Customers { get; set; }
        public DbSet<BankAccount> BankAccounts { get; set; }
        public DbSet<Deposit> Deposits { get; set; }
        public DbSet<WithDrawal> Withdrawals { get; set; }
        public DbSet<Loan> Loans { get; set; }

        public DbSet<DataManipulation> DataManipulations { get; set; }
        public DbSet<User> Users { get; set; }

    }
}
