﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for main_loan_page.xaml
    /// </summary>
    public partial class main_loan_page : Page
    {

        public main_loan_page()
        {
           InitializeComponent();
            loan_page_frame.Content = new full_member_page();
        }

        private void full_member_Click(object sender, RoutedEventArgs e)
        {
            loan_page_frame.Content = new full_member_page();

            full_member.Background = Brushes.LightSeaGreen;
            full_member.Foreground = Brushes.White;

            waiting_member.Foreground = Brushes.DodgerBlue;
            waiting_member.Background = Brushes.White;

            non_member.Foreground = Brushes.DodgerBlue;
            non_member.Background = Brushes.White;
        }

        private void waiting_member_Click(object sender, RoutedEventArgs e)
        {
            loan_page_frame.Content = new waiting_member_page();

            waiting_member.Background = Brushes.LightSeaGreen;
            waiting_member.Foreground = Brushes.White;

            full_member.Foreground = Brushes.DodgerBlue;
            full_member.Background = Brushes.White;

            non_member.Foreground = Brushes.DodgerBlue;
            non_member.Background = Brushes.White;
        }


        private void non_member_Click(object sender, RoutedEventArgs e)
        {
            loan_page_frame.Content = new non_member_page();
            non_member.Foreground = Brushes.LightSeaGreen;
            non_member.Background = Brushes.White;

            full_member.Foreground = Brushes.DodgerBlue;
            full_member.Background = Brushes.White;

            waiting_member.Foreground = Brushes.DodgerBlue;
            waiting_member.Background = Brushes.White;
        }
    }
}
