﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for full_info.xaml
    /// </summary>
    public partial class full_info : Page
    {
        public full_info()
        {
            InitializeComponent();
        }


        private void loan_button_Click(object sender, RoutedEventArgs e)
        {
            full_member_page go = new full_member_page();

            NavigationService.Navigate(go);
            full_info full = new full_info();
            full.ShowsNavigationUI = true;
        }
    }
}
