﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;


namespace Cusoft
{
    /// <summary>
    /// Interaction logic for graph_charts_page.xaml
    /// </summary>
    public partial class graph_charts_page : Page
    {
        public graph_charts_page()
        {
            InitializeComponent();
            this.Demands = new ObservableCollection<GoldDemand>
{
new GoldDemand()
{
Demand = "Jewelry", Year2010 = 1998.0, Year2011 = 2361.2
},
new GoldDemand()
{
Demand = "Electronics",Year2010 = 1284.0, Year2011 = 1328.0
},
new GoldDemand()
{
Demand = "Research",Year2010 = 1090.5, Year2011 = 1032.0
},
new GoldDemand()
{
Demand = "Investment",Year2010 = 1643.0, Year2011 = 1898.0
},
new GoldDemand()
{
Demand = "Bank Purchases", Year2010 = 987.0, Year2011 = 887.0
}
};
            this.DataContext = this;
        }




        public ObservableCollection<GoldDemand> Demands { get; set; }

        public class GoldDemand
        {
            public string Demand { get; set; }
            public double Year2010 { get; set; }
            public double Year2011 { get; set; }
        }

    }
}
