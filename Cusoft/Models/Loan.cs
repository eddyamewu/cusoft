﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cusoft.Models
{
    public class Loan
    {
        public Loan()
        {
            ID = Guid.NewGuid();
            Created = DateTime.Now;
        }

        public Guid ID { get; set; }

        [MaxLength(256)]
        public string StaffId { get; set; }

        public double AmountApplying { get; set; }
        public double AmountApproved { get; set; }
        public int DurationInMonths { get; set; }
        public double AmountPaying { get; set; }
        public double InterestAmount { get; set; }

        public DateTime Created { get; set; }

        //[MaxLength(256)]
        //public string GuarantorId { get; set; }

        //[MaxLength(256)]
        //public string GuarantorName { get; set; }

        //[MaxLength(256)]
        //public string GuarantorContact { get; set; }

        //[MaxLength(256)]
        //public string GuarantorAddress { get; set; }




    }
}
