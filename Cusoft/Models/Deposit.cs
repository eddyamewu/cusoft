﻿namespace Cusoft.Models
{
    public class Deposit
    {
        public int Id { get; set; }
        public string AccountNumber { get; set; }
        public double Amount { get; set; }
    }
}
