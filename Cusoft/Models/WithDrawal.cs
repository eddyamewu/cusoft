﻿namespace Cusoft.Models
{
    public class WithDrawal
    {
        public int Id { get; set; }
        public int MyProperty { get; set; }
        public string AccountNumber { get; set; }
        public double Amount { get; set; }
    }
}
