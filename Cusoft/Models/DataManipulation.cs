﻿using System;

namespace Cusoft.Models
{
    public class DataManipulation
    {
        public DataManipulation()
        {
            // Id = Guid.NewGuid();
            Created = DateTime.Now;
        }

        public int Id { get; set; }
        public double InterestRateForFullMembers { get; set; }
        public double InterestRateForNonMembers { get; set; }
        public double InterestRateForWaitingMembers { get; set; }
        public double AmountPerShare { get; set; }
        public int FullMemberShipTimeRange { get; set; }
        public double WithdrawalPercentageAllowed { get; set; }
        public DateTime Created { get; set; }
    }
}
