﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cusoft.Models
{
    class NonMemberLoan
    {

        public NonMemberLoan()
        {
            ID = Guid.NewGuid();
            Created = DateTime.Now;
        }

        public Guid ID { get; set; }

        [MaxLength(256)]
        public string StaffId { get; set; }

        public decimal AmountApplying { get; set; }
        public decimal AmountApproved { get; set; }
        public int DurationInMonths { get; set; }
        public decimal AmountPaying { get; set; }
        public decimal InterestAmount { get; set; }


        [MaxLength(256)]
        public string GuarantorId { get; set; }

        [MaxLength(256)]
        public string GuarantorName { get; set; }

        [MaxLength(256)]
        public string GuarantorContact { get; set; }

        [MaxLength(256)]
        public string GuarantorAddress { get; set; }

        public DateTime Created { get; set; }
    }
}
