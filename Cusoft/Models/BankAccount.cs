﻿using System;
using System.Collections.Generic;

namespace Cusoft.Models
{
    public class BankAccount
    {
        public BankAccount()
        {
            Id = Guid.NewGuid();
            Created = DateTime.UtcNow;
        }
        public Guid Id { get; set; }
        public string AccountNumber { get; set; }
        public double Balance { get; set; }
        public int Shares { get; set; }
        public double SharesPercentage { get; set; }
        public AccountStatus AccountStatus { get; set; } = AccountStatus.Active;
        public ICollection<Deposit> Deposits { get; set; }
        public ICollection<WithDrawal> WithDrawals { get; set; }
        public ICollection<Loan> Loans { get; set; }

        public DateTime Created { get; set; }
    }

    public enum AccountStatus
    {
        Active = 1,
        InActive = 2
    }
}
