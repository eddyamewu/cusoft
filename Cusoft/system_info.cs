﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cusoft
{
    class system_info
    {

        //withdrawal fees for the customers
        private double withdrawal_fees;

        public double _withdrawal_fees
        {
            get
            {
                return withdrawal_fees;
            }

            set
            {
                if (withdrawal_fees < 0)
                {
                    withdrawal_fees = 0;
                }
                else
                {
                    withdrawal_fees = value;
                }

            } //end of setter
        } // end of property




        // the minimum days from the last withdrawal
        private int min_days_to_withdraw;

        public int _min_days_to_withdraw
        {
            get
            {
                return min_days_to_withdraw;
            }

            set
            {
                if(min_days_to_withdraw<0)
                {
                    min_days_to_withdraw = 0;
                }
                else
                {
                    min_days_to_withdraw = value;
                }
            } //end of setter
        } // end of property




        // maximum percentage to be withdrawn out of the total amount of contributions
        private double max_perc_to_be_withdrawn;

        public double _max_perc_to_be_withdrawn
        {
            get
            {
                return max_perc_to_be_withdrawn;
            }

            set
            {
                if (max_perc_to_be_withdrawn > 100)
                {
                    max_perc_to_be_withdrawn = 100;
                }
                else
                {
                    max_perc_to_be_withdrawn = value;
                }
            } //end of setter
        } // end of property




        //price per share of the contributions (share account)
        private double price_per_share;

        public double _price_per_share
        {
            get
            {
                return price_per_share;
            }

            set
            {
                price_per_share = value;
            } //end of setter
        } // end of property




        public double one()
        {
            _price_per_share = 50;
            return _price_per_share;
        }
    }
}
