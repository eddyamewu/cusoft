﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Cusoft.Services;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for create_acc_page.xaml
    /// </summary>
    public partial class create_acc_page : Page
    {
        public create_acc_page()
        {
            InitializeComponent();
        }



        private void browse_for_image_button_click(object sender, RoutedEventArgs e)
        {

        }



        private void create_acc_page_clear_button_click(object sender, RoutedEventArgs e)
        {
           // when nothing is entered, it is blur. when data is entered, it shows
        }


        private void create_acc_page_cancel_button_click(object sender, RoutedEventArgs e)
        {
            main_start_page nav = new main_start_page();
            NavigationService.Navigate(nav);

            /*when nothing entered, it will close. when something entered, it will ask to save work as draft and
             continue later before closing*/

  }



        private void create_acc_page_save_button_click(object sender, RoutedEventArgs e)
        {

            CustomerServices serve = new CustomerServices();

            try
            {
                serve.AddToCustomersTable();
                MessageBox.Show("Account Created Successfully!");
            }
            catch
            {
                MessageBox.Show("Can't be save!! \n Try Entring the Data again");
            }


            /*when data not entered, it is blur when all required fields entered, it is shown
            when pressed, it pops up asking to save ( sure? ). if yes, continue. if no, the popup closes and stays so*/
        }






        /*look out to plan an algorithm for generating the account number

        private void create_acc_page_edit_button_click(object sender, RoutedEventArgs e)
        {
            when nothing is saved at at drafts yet, it is blur. show when a draft work is resumed
        }
        */
    }
}
