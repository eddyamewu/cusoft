namespace Cusoft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class initial : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Accounts", newName: "Account");
            RenameTable(name: "dbo.Deposits", newName: "Deposit");
            RenameTable(name: "dbo.Loans", newName: "Loan");
            RenameTable(name: "dbo.WithDrawals", newName: "WithDrawal");
            RenameTable(name: "dbo.Customers", newName: "Customer");
            RenameTable(name: "dbo.DataManipulations", newName: "DataManipulation");
            RenameTable(name: "dbo.Users", newName: "User");
            AddColumn("dbo.Customer", "Gender", c => c.String());
            DropColumn("dbo.Customer", "DateOfBirth");
            DropColumn("dbo.Customer", "JobTittle");
            DropColumn("dbo.Customer", "SsnitId");
            DropColumn("dbo.Customer", "Faculty");
            DropColumn("dbo.Customer", "Department");
            DropColumn("dbo.Customer", "WorkShift");
            DropColumn("dbo.Customer", "ContactNumber");
            DropColumn("dbo.Customer", "EmailAddress");
            DropColumn("dbo.Customer", "ResidentialAddress");
            DropColumn("dbo.Customer", "BeneficiaryName");
            DropColumn("dbo.Customer", "BeneficiaryContact");
            DropColumn("dbo.Customer", "NextOfKinName");
            DropColumn("dbo.Customer", "NextOfKinContact");
        }

        public override void Down()
        {
            AddColumn("dbo.Customer", "NextOfKinContact", c => c.String());
            AddColumn("dbo.Customer", "NextOfKinName", c => c.String());
            AddColumn("dbo.Customer", "BeneficiaryContact", c => c.String());
            AddColumn("dbo.Customer", "BeneficiaryName", c => c.String());
            AddColumn("dbo.Customer", "ResidentialAddress", c => c.String());
            AddColumn("dbo.Customer", "EmailAddress", c => c.String());
            AddColumn("dbo.Customer", "ContactNumber", c => c.String());
            AddColumn("dbo.Customer", "WorkShift", c => c.String());
            AddColumn("dbo.Customer", "Department", c => c.String());
            AddColumn("dbo.Customer", "Faculty", c => c.String());
            AddColumn("dbo.Customer", "SsnitId", c => c.String());
            AddColumn("dbo.Customer", "JobTittle", c => c.String());
            AddColumn("dbo.Customer", "DateOfBirth", c => c.DateTime());
            DropColumn("dbo.Customer", "Gender");
            RenameTable(name: "dbo.User", newName: "Users");
            RenameTable(name: "dbo.DataManipulation", newName: "DataManipulations");
            RenameTable(name: "dbo.Customer", newName: "Customers");
            RenameTable(name: "dbo.WithDrawal", newName: "WithDrawals");
            RenameTable(name: "dbo.Loan", newName: "Loans");
            RenameTable(name: "dbo.Deposit", newName: "Deposits");
            RenameTable(name: "dbo.Account", newName: "Accounts");
        }
    }
}
