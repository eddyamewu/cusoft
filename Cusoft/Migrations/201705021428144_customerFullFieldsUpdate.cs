namespace Cusoft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class customerFullFieldsUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customer", "MiddleName", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "DateOfBirth", c => c.DateTime());
            AddColumn("dbo.Customer", "JobTittle", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "SsnitId", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "Faculty", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "Department", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "WorkShift", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "ContactNumber", c => c.String(maxLength: 50));
            AddColumn("dbo.Customer", "EmailAddress", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "ResidentialAddress", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "BeneficiaryName", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "BeneficiaryContact", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "NextOfKinName", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "NextOfKinContact", c => c.String(maxLength: 256));
            AlterColumn("dbo.Customer", "FirstName", c => c.String(maxLength: 256));
            AlterColumn("dbo.Customer", "LastName", c => c.String(maxLength: 256));
        }

        public override void Down()
        {
            AlterColumn("dbo.Customer", "LastName", c => c.String());
            AlterColumn("dbo.Customer", "FirstName", c => c.String());
            DropColumn("dbo.Customer", "NextOfKinContact");
            DropColumn("dbo.Customer", "NextOfKinName");
            DropColumn("dbo.Customer", "BeneficiaryContact");
            DropColumn("dbo.Customer", "BeneficiaryName");
            DropColumn("dbo.Customer", "ResidentialAddress");
            DropColumn("dbo.Customer", "EmailAddress");
            DropColumn("dbo.Customer", "ContactNumber");
            DropColumn("dbo.Customer", "WorkShift");
            DropColumn("dbo.Customer", "Department");
            DropColumn("dbo.Customer", "Faculty");
            DropColumn("dbo.Customer", "SsnitId");
            DropColumn("dbo.Customer", "JobTittle");
            DropColumn("dbo.Customer", "DateOfBirth");
            DropColumn("dbo.Customer", "MiddleName");
        }
    }
}
