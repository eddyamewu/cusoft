﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for main_fin_state_page.xaml
    /// </summary>
    public partial class main_fin_state_page : Page
    {
        public main_fin_state_page()
        {
            InitializeComponent();

            List<fin_state> fin_states = new List<fin_state>();

            fin_states.Add(new fin_state() { Transaction = "MEMBERSHIP ACCOUNT DEPOSIT", Total_amt = fin.amt });
            fin_states.Add(new fin_state() { Transaction = "MEMBERSHIP ACCOUNT WITHDRAWAL", Total_amt = fin.amt });
            fin_states.Add(new fin_state() { Transaction = "LOANS PROCESSED", Total_amt = fin.amt });
            fin_states.Add(new fin_state() { Transaction = "LOAN INTEREST", Total_amt = fin.amt });
            fin_states.Add(new fin_state() { Transaction = "CUSTOMER ACCOUNT BALANCE", Total_amt = fin.amt });

            datagrid.ItemsSource = fin_states;
        }



        public class fin_state
        {
            public string Transaction { get; set; }

            public string Total_amt { get; set; }
        }


        public class fin
        {
            public static string amt = "Ghc. 1000";
        }

    }
}
