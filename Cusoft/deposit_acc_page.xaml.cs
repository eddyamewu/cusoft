﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for deposit_acc_page.xaml
    /// </summary>
    public partial class deposit_acc_page : Page
    {
        public deposit_acc_page()
        {
            InitializeComponent();
        }

        private void deposit_acc_search_button_click(object sender, RoutedEventArgs e)
        {
            deposit_page_search_match_frame.Content = new deposit_search_match_no();
        }
    }
}
