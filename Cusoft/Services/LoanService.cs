﻿using Cusoft.Data;
using Cusoft.Models;
using System;
using System.Linq;

namespace Cusoft.Services
{
    public class LoanService
    {
        private readonly DataBaseContext _context;

        public LoanService()
        {
            _context = new DataBaseContext();
        }

        public void SaveLoan(Guid customerId, Guid bankAccountId, Loan loan)
        {
            var bankAccount = _context.BankAccounts.FirstOrDefault(t => t.Id == bankAccountId);
            var dataManipulation = _context.DataManipulations.FirstOrDefault();

            switch (MemberLevelShip(customerId))
            {
                case MemberLevel.FullMember:
                    {
                        loan.InterestAmount = loan.AmountApproved *
                            dataManipulation.InterestRateForFullMembers * loan.DurationInMonths;
                        break;
                    }
                case MemberLevel.WaitingMember:
                    {
                        loan.InterestAmount = loan.AmountApproved *
                            dataManipulation.InterestRateForWaitingMembers * loan.DurationInMonths;
                        break;
                    }
                case MemberLevel.NonMember:
                    {
                        loan.InterestAmount = loan.AmountApproved *
                            dataManipulation.InterestRateForNonMembers * loan.DurationInMonths;
                        break;
                    }
            }

            bankAccount.Loans.Add(loan);
            _context.SaveChanges();
        }

        private MemberLevel MemberLevelShip(Guid customerId)
        {
            if (customerId == Guid.Empty)
            {
                return MemberLevel.NonMember;
            }
            else
            {
                var sixMounthsTime = _context.Customers.FirstOrDefault(t => t.Id == customerId)
                .Created.AddMonths(6);

                return (DateTime.Now > sixMounthsTime) ? MemberLevel.FullMember : MemberLevel.WaitingMember;
            }

        }
    }

    public enum MemberLevel
    {
        FullMember,
        WaitingMember,
        NonMember
    }
}
