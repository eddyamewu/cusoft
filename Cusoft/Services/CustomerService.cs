﻿using Cusoft.Data;
using Cusoft.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Cusoft.Services
{
    public class CustomerService
    {
        private readonly DataBaseContext _context;

        public CustomerService()
        {
            _context = new DataBaseContext();
        }

        public IEnumerable<Customer> GetAllCustomers()
        {
            var customers = _context.Customers.Include(t => t.BankAccounts).ToList();
            return customers;
        }

        public Customer GetAllCustomerById(Guid id)
        {
            var customer = _context.Customers.Include(t => t.BankAccounts).FirstOrDefault(t => t.Id == id);
            return customer;
        }

        public void DeleteCustomer(Guid id)
        {
            var customer = GetAllCustomerById(id);
            _context.Customers.Remove(customer);
            _context.SaveChanges();
        }

        public void UpdateCustomer(Guid id, Customer customer)
        {
            var originalCustomer = GetAllCustomerById(id);

            originalCustomer.DateOfBirth = customer.DateOfBirth;
            originalCustomer.JobTittle = customer.JobTittle;
            originalCustomer.SsnitId = customer.SsnitId;
            originalCustomer.Department = customer.Department;
            originalCustomer.ContactNumber = customer.ContactNumber;
            originalCustomer.EmailAddress = customer.EmailAddress;
            originalCustomer.ResidentialAddress = customer.ResidentialAddress;
            originalCustomer.NextOfKinName = customer.NextOfKinName;
            originalCustomer.NextOfKinContact = customer.NextOfKinContact;

            _context.SaveChanges();
        }

    }
}
