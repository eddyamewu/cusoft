﻿using Cusoft.Data;
using Cusoft.Models;
using System;

namespace Cusoft.Services
{
    class CustomerServices
    {
        private readonly create_acc_page Invoke = new create_acc_page();

        //adding new information to the customer table
        public void AddToCustomersTable()
        {

            using (DataBaseContext context = new DataBaseContext())
            {
                String CustomGender;

                //setting customer gender with condition
                CustomGender = Invoke.MaleGenderOption.Text == "Male" ? "Male" : "Female";

                context.Customers.Add(new Customer
                {
                    OtherNames = Invoke.OtherNamesTextBox.Text,
                    SurName = Invoke.SurNameTextBox.Text,
                    Gender = CustomGender
                });

                context.SaveChanges();
                Console.WriteLine("Successfully saved!!");
            }
        } //end of method

    }
}
