﻿using Cusoft.Data;
using Cusoft.Models;
using System.Linq;

namespace Cusoft.Services
{
    public class UserService
    {
        private readonly DataBaseContext _context;

        public UserService()
        {
            _context = new DataBaseContext();
        }

        public void Register(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
        }
        public bool Login(User user)
        {
            return _context.Users.Any(t => t.UserName == user.UserName)
                && _context.Users.Any(t => t.Password == user.Password)
                    ? true : false;
        }
    }
}
