﻿using Cusoft.Data;
using Cusoft.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cusoft.Services
{
    public class DataManipulationService
    {
        private readonly DataBaseContext _context;

        public DataManipulationService()
        {
            _context = new DataBaseContext();
        }

        public IList<DataManipulation> GetDataManipulation()
        {
            try
            {
                var dataManipulations = _context.DataManipulations.ToList();
                return dataManipulations;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataManipulation GetDataManipulationById(int id)
        {
            try
            {
                var dataManipulation = _context.DataManipulations.FirstOrDefault(t => t.Id == id);
                return dataManipulation;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void SaveDataManipulation(DataManipulation dataManipulation)
        {
            try
            {
                if (dataManipulation != null)
                {
                    _context.DataManipulations.Add(dataManipulation);
                    _context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateDataManipulation(int id, DataManipulation dataManipulation)
        {
            try
            {
                var originalDataManipulation = _context.DataManipulations.FirstOrDefault(t => t.Id == id);

                originalDataManipulation.AmountPerShare = dataManipulation.AmountPerShare;
                originalDataManipulation.WithdrawalPercentageAllowed = dataManipulation.WithdrawalPercentageAllowed;

                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteDataManipulation(int id)
        {
            try
            {
                var dataManipulation = _context.DataManipulations.FirstOrDefault(t => t.Id == id);
                _context.DataManipulations.Remove(dataManipulation);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
