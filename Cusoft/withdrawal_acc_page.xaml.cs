﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for withdrawal_acc_page.xaml
    /// </summary>
    public partial class withdrawal_acc_page : Page
    {
        public withdrawal_acc_page()
        {
            InitializeComponent();
        }

        private void withdrawal_acc_search_button_click(object sender, RoutedEventArgs e)
        {
            withdrawal_page_search_match_frame.Content = new withdrawal_search_match_yes();
        }
    }
}
